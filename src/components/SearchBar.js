import React, { useRef } from 'react';
import { MdSearch, MdClose } from 'react-icons/md';
import PropTypes from 'prop-types';

/* redux */
import { connect } from 'react-redux';
import { searchLogs } from '../actions/logActions';

const SearchBar = ({ searchLogs }) => {
	const text = useRef('');

	const onChange = e => {
		searchLogs(text.current.value);
	};

	return (
		<nav style={{ marginBottom: '30px' }} className="blue">
			<div className="nav-wrapper">
				<form>
					<div className="input-field">
						<input
							type="search"
							id="search"
							placeholder="Search logs..."
							ref={text}
							onChange={onChange}
						/>
						<label htmlFor="search" className="label-icon">
							<MdSearch className="material-icons white-text">search</MdSearch>
						</label>
						<MdClose className="material-icons">close</MdClose>
					</div>
				</form>
			</div>
		</nav>
	);
};

SearchBar.propTypes = {
	searchLogs: PropTypes.func.isRequired
};

const mapDispatchToProps = dispatch => ({
	searchLogs: text => dispatch(searchLogs(text))
});

export default connect(
	null,
	mapDispatchToProps
)(SearchBar);
