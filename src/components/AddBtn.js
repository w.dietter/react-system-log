import React from 'react';
import { MdAdd, MdPerson, MdPersonAdd } from 'react-icons/md';

const AddBtn = () => {
	return (
		<div className="fixed-action-btn">
			<a
				href="#add-log-modal"
				className="btn-floating blue btn-large darken-2 modal-trigger"
			>
				<MdAdd />
			</a>
			<ul>
				<li>
					<a
						href="#tech-list-modal"
						className="btn-floating green modal-trigger"
					>
						<MdPerson />
					</a>
				</li>
				<li>
					<a href="#tech-modal" className="btn-floating red modal-trigger">
						<MdPersonAdd />
					</a>
				</li>
			</ul>
		</div>
	);
};

export default AddBtn;
