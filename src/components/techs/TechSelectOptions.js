import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
/* redux */
import { connect } from 'react-redux';
import { getTechs } from '../../actions/techActions';

const TechSelectOptions = ({ getTechs, tech: { techs, loading } }) => {
	useEffect(() => {
		// eslint-disable-next-line
	}, [techs]);

	return (
		<>
			{!loading &&
				techs !== null &&
				techs.map(t => (
					<option key={t.id} value={`${t.firstName} ${t.lastName}`}>
						{t.firstName} {t.lastName}
					</option>
				))}
		</>
	);
};

TechSelectOptions.propTypes = {};

const mapStateToProps = state => ({
	tech: state.tech
});

TechSelectOptions.propTypes = {
	getTechs: PropTypes.func.isRequired,
	tech: PropTypes.object.isRequired
};

const mapDispatchToProps = dispatch => ({
	getTechs: () => dispatch(getTechs())
});
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(TechSelectOptions);
