import React from 'react';
import PropTypes from 'prop-types';
import { MdDelete } from 'react-icons/md';
import M from 'materialize-css/dist/js/materialize.min.js';
/* redux */
import { connect } from 'react-redux';
import { deleteTech } from '../../actions/techActions';

const TechItem = ({ tech, deleteTech }) => {
	const onDelete = () => {
		deleteTech(tech.id);
		M.toast({ html: `${tech.firstName} ${tech.lastName} was deleted.` });
	};
	return (
		<li className="collection-item">
			<div>
				{tech.firstName} {tech.lastName}
				<a href="#!" className="secondary-content black-text">
					<MdDelete onClick={onDelete} />
				</a>
			</div>
		</li>
	);
};

TechItem.propTypes = {
	tech: PropTypes.object.isRequired,
	deleteTech: PropTypes.func.isRequired
};

const mapDispatchToProps = dispatch => ({
	deleteTech: id => dispatch(deleteTech(id))
});

export default connect(
	null,
	mapDispatchToProps
)(TechItem);
