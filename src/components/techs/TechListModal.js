import React, { useEffect } from 'react';
import TechItem from './TechItem';
import PropTypes from 'prop-types';

/* redux */
import { connect } from 'react-redux';
import { getTechs } from '../../actions/techActions';

const TechListModal = ({ getTechs, tech: { techs, loading } }) => {
	console.log(techs);
	console.log(loading);
	useEffect(() => {
		getTechs();
		// eslint-disable-next-line
	}, []);

	return (
		<div id="tech-list-modal" className="modal">
			<div className="modal-content">
				<h4>Technician List</h4>
				<ul className="collection">
					{!loading &&
						(techs !== null &&
							techs.map(tech => <TechItem key={tech.id} tech={tech} />))}
				</ul>
			</div>
		</div>
	);
};

TechListModal.propTypes = {
	tech: PropTypes.object.isRequired,
	getTechs: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
	tech: state.tech
});

const mapDispatchToProps = dispatch => ({
	getTechs: () => dispatch(getTechs())
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(TechListModal);
