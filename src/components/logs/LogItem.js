import React from 'react';
import Moment from 'react-moment';
import PropTypes from 'prop-types';
import M from 'materialize-css/dist/js/materialize.min.js';
/* components */
import { MdDelete } from 'react-icons/md';
/* redux */
import { connect } from 'react-redux';
import { deleteLog, setCurrent } from '../../actions/logActions';

const LogItem = ({ log, deleteLog, setCurrent }) => {
	const onDelete = () => {
		deleteLog(log.id);
		M.toast({ html: 'Log Deleted' });
	};

	const onEditLogModalOpen = () => {
		setCurrent(log);
	};

	return (
		<li className="collection-item">
			<div>
				<a
					href="#edit-log-modal"
					className={`modal-trigger ${
						log.attention ? 'red-text' : 'blue-text'
					}`}
					onClick={onEditLogModalOpen}
				>
					{log.message}
				</a>
				<br />
				<span className="grey-text">
					<span className="black-text">ID #{log.id} </span> last updated by{' '}
					<span className="black-text">{log.tech}</span> on{' '}
					<Moment format="MMMM Do YYYY, h:mm:ss a">{log.date}</Moment>
				</span>
				<a href="#!" className="secondary-content">
					<MdDelete className="grey-text" onClick={onDelete}>
						Delete
					</MdDelete>
				</a>
			</div>
		</li>
	);
};

LogItem.propTypes = {
	log: PropTypes.object.isRequired,
	deleteLog: PropTypes.func.isRequired,
	setCurrent: PropTypes.func.isRequired
};

const mapDispatchToProps = dispatch => ({
	deleteLog: id => dispatch(deleteLog(id)),
	setCurrent: log => dispatch(setCurrent(log))
});

export default connect(
	null,
	mapDispatchToProps
)(LogItem);
