import React from 'react';
import 'materialize-css/dist/css/materialize.min.css';
import M from 'materialize-css/dist/js/materialize.min.js';
/* components */
import SearchBar from './components/SearchBar';
import Logs from './components/logs/Logs';
import AddBtn from './components/AddBtn';
import AddLogModal from './components/logs/AddLogModal';
import EditLogModal from './components/logs/EditLogModal';
import AddTechModal from './components/techs/AddTechModal';
import TechListModal from './components/techs/TechListModal';
/* redux */
import store from './store';
import { Provider } from 'react-redux';

const App = () => {
	React.useEffect(() => {
		M.AutoInit();
		// eslint-disable-next-line
	}, []);
	return (
		<>
			<Provider store={store}>
				<AddLogModal />
				<EditLogModal />
				<AddTechModal />
				<TechListModal />
				<SearchBar />
				<div className="container">
					<Logs />
				</div>
				<AddBtn />
			</Provider>
		</>
	);
};

export default App;
