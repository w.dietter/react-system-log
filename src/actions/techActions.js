import { TechActionTypes } from './types';

export const setLoading = () => {
	return {
		type: TechActionTypes.SET_LOADING
	};
};

export const getTechs = () => async dispatch => {
	try {
		setLoading();

		const res = await fetch('/techs');
		const data = await res.json();
		dispatch({
			type: TechActionTypes.GET_TECHS,
			payload: data
		});
	} catch (error) {
		dispatch({
			type: TechActionTypes.TECHS_ERROR,
			payload: error
		});
	}
};

export const addTech = tech => async dispatch => {
	try {
		setLoading();

		const res = await fetch('/techs', {
			method: 'POST',
			body: JSON.stringify(tech),
			headers: {
				'Content-Type': 'application/json'
			}
		});
		const data = await res.json();
		dispatch({
			type: TechActionTypes.ADD_TECH,
			payload: data
		});
	} catch (error) {
		dispatch({
			type: TechActionTypes.TECHS_ERROR,
			payload: error
		});
	}
};

export const deleteTech = id => async dispatch => {
	try {
		setLoading();
		await fetch(`/techs/${id}`, { method: 'DELETE' });
		dispatch({
			type: TechActionTypes.DELETE_TECH,
			payload: id
		});
	} catch (error) {
		dispatch({
			type: TechActionTypes.TECHS_ERROR,
			payload: error
		});
	}
};
